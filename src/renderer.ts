/**
 * This is the renderer entry point loaded by the electron browser window.
 */

import "./index.css";

import FiniteAutomaton, { FAResultStatus } from "./FiniteAutomaton";
import { existsSync, readFileSync, writeFileSync } from "original-fs";

import { Console } from "console";
import { resolve } from "path";

// create a remote console so we can output to the console instead of the window.
const remoteConsole = new Console(process.stdout, process.stderr);

// get the current working directory the program was started in.
const cwd = process.cwd();

// get the file paths from the environment variables

const SPEC_FILE = process.env["SPEC_FILE"];
const STRINGS_FILE = process.env["STRINGS_FILE"];
const ANSWERS_FILE = process.env["ANSWERS_FILE"];

// log the file path variables
remoteConsole.log(`SPEC_FILE="${SPEC_FILE}"`);
remoteConsole.log(`STRINGS_FILE="${STRINGS_FILE}"`);
remoteConsole.log(`ANSWERS_FILE="${ANSWERS_FILE}"`);

// check if each file exists

let hasspecfile = false;
let hasstringfile = false;
let hasanswersfilepath = false;

let specfilepath, stringsfilepath, answersfilepath;

if (SPEC_FILE) {
	specfilepath = resolve(cwd, SPEC_FILE);
	hasspecfile = existsSync(specfilepath);
}
if (STRINGS_FILE) {
	stringsfilepath = resolve(cwd, STRINGS_FILE);
	hasstringfile = existsSync(stringsfilepath);
}
if (ANSWERS_FILE) {
	answersfilepath = resolve(cwd, ANSWERS_FILE);
	hasanswersfilepath = existsSync(stringsfilepath);
}

// create the finite automaton
const fa = new FiniteAutomaton();

// if the spec file exists
if (hasspecfile) {
	// try parsing the spec file

	try {
		// read the contents of the file
		const rawspecfile = readFileSync(specfilepath).toString();

		const specfile = rawspecfile
			// split the file into an array of lines
			.split("\n")
			// trim leading and trailing whitespace
			.map((line) => line.trim())
			// remove empty lines
			.filter((s) => s.length);

		// find the line containing the states
		const states = specfile.find((line) => line.startsWith("States"));

		// register the states with the finite automaton
		fa.setStates(
			states
				// get the substring between the brackets
				.slice(states.indexOf("{") + 1, states.lastIndexOf("}"))
				// split the string at each comma
				.split(",")
				// trim whitespace
				.map((s) => s.trim())
		);

		// find the line containing the alphabet
		const alpha = specfile.find((line) => line.startsWith("Alphabet"));

		// register the alphabet with the finite automaton
		fa.setAlphabet(
			alpha
				// get the substring between the brackets
				.slice(states.indexOf("{") + 1, states.lastIndexOf("}"))
				// split the string at each comma
				.split(",")
				// trim whitespace
				.map((s) => s.trim())
		);

		// find the line containing the starting state
		const starting = specfile.find((line) => line.startsWith("Starting State"));

		// register the entry state with the finite automaton
		fa.setEntryState(
			starting
				// get the substring after the equals
				.slice(starting.indexOf("=") + 1, starting.indexOf(","))
				// trim whitespace
				.trim()
		);

		// find the line containing the final states
		const final = specfile.find((line) => line.startsWith("Final States"));

		// register the final states with the finite automaton
		fa.setFinalStates(
			final
				// get the substring between the brackets
				.slice(final.indexOf("{") + 1, final.lastIndexOf("}"))
				// split the string at each comma
				.split(",")
				// trim whitespace
				.map((s) => s.trim())
		);

		// find the line index where the transitions start
		const transitionIndex = specfile.findIndex((line) =>
			line.startsWith("Transition")
		);

		// for each line after the transition start line
		for (let i = transitionIndex + 1; i < specfile.length; i++) {
			// split the entries by commas
			const cur = specfile[i].split(",");

			// if line does not contain a transition
			// the we have reached the end of the transitions list
			if (cur.length < 3) break;

			// register the transition with the finite automaton
			fa.addTransition(
				// from
				cur[0].slice(1).trim(),
				// transition value
				cur[1].trim(),
				// to
				cur[2].slice(0, cur[2].lastIndexOf(")")).trim()
			);
		}

		// render the finite automaton

		// global graph scale
		const scale = 1;

		// add extra padding around the graph
		const rect = fa.bounds.expand(64 * scale);

		// get the canvas width from the graph size and scale it to match
		const width = rect.width * scale;
		const height = rect.height * scale;

		// get the main div element
		const main = document.getElementById("canvas-container");

		// create a new canvas element
		const canvas = document.createElement("canvas");

		// set the canvas size
		canvas.width = width;
		canvas.height = height;

		// add the canvas to the DOM in the canvas container
		main.appendChild(canvas);

		// get the rendering context of the canvas element
		const ctx = canvas.getContext("2d");

		// set the background of the canvas to white
		ctx.fillStyle = "white";
		ctx.fillRect(0, 0, width, height);

		// setup the fill style for the graph
		ctx.fillStyle = "black";

		// pre scale the rendering context
		ctx.scale(scale, scale);
		// shift the canvas so that the graph rect is aligned
		ctx.translate(-rect.left, -rect.top);

		// draw the finite automaton
		fa.draw(ctx);

		// reset all of the transforms
		ctx.resetTransform();
	} catch (ex) {
		remoteConsole.log("failed to load input file");
		remoteConsole.trace(ex);
	}
} else {
	remoteConsole.log("no input string file specified");
}

// if there is a spec file and a string input file
if (hasspecfile && hasstringfile) {
	// no output file specified
	if (!hasanswersfilepath) {
		remoteConsole.log("no output file path specified");
	} else {
		// read the input string file
		const rawstrings = readFileSync(stringsfilepath)
			.toString()
			// split the file into an array of lines
			.split("\n")
			// trim leading and trailing whitespace
			.map((line) => line.trim())
			// remove empty lines
			.filter((s) => s.length);

		// output answers file buffer
		let outputBuffer = "";

		// for each input string from the file
		rawstrings.forEach((x) => {
			// add a new line
			outputBuffer += "\n";
			// add the input string
			outputBuffer += x;
			// add the result
			outputBuffer +=
				// process the string with the finite automaton
				// and add the result
				fa.process(x).status === FAResultStatus.ACCEPTED
					? " is accepted.\n"
					: " is rejected.\n";
		});

		// write the output buffer to the answers file
		writeFileSync(answersfilepath, outputBuffer);

		// log that we successfully processed the input and wrote the answers
		// to a file
		remoteConsole.log(`answers written to file ${answersfilepath}`);
	}
} else {
	remoteConsole.log("no strings input file specified");
}
