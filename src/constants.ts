/**
 * Global Constants used for the renderer
 */

export const LINE_WIDTH = 2;
export const FONT_FACE = "serif";
export const FONT_ALPHA = "24px " + FONT_FACE;
export const FONT_STATE = "bold 24px " + FONT_FACE;
export const NODE_SIZE = 64;
export const NODE_DISTANCE = NODE_SIZE * 3;
export const NODE_ENTRY_ARROW_SIZE = NODE_SIZE * 0.2;
export const ARROW_SIZE = NODE_SIZE * 0.1;
export const ARROW_PADDING = LINE_WIDTH * 3;
export const ARROW_LABEL_DIST = LINE_WIDTH + 18;
