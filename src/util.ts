/**
 * Utility data types and functions.
 *
 * Most of these functions are here to make calculations in the renderer easier
 * to code and read.
 *
 * The operations in the following classes are designed for chaining.
 * Past-tense named operations are shortcuts for cloning the point before
 * performing the operation.```point.scaled(x,y) -> point.clone().scale(x,y)```
 *
 * ```
 * const myPoint = myStartingPoint
 * 		// clones myStartingPoint and rotates it.
 * 		.rotated(Math.PI * 0.25)
 * 		// scales rotated point by 123
 * 		.scale(123)
 * 		// translates the point by the other point values.
 * 		.translate(otherPoint.x, otherPoint.y);
 * ```
 *
 */

/**
 * Point object
 *
 * A 2D point where x represents the horizontal axis and y represents the
 * vertical axis.
 *
 * @export
 * @class Point
 */
export class Point {
	/**
	 * point position on the x axis.
	 *
	 * @type {number}
	 * @memberof Point
	 */
	x: number;

	/**
	 * point position on the y axis.
	 *
	 * @type {number}
	 * @memberof Point
	 */
	y: number;

	/**
	 * Creates an instance of Point with the given x and y values.
	 * @param {number} x position on the x axis.
	 * @param {number} y position on the y axis.
	 * @memberof Point
	 */
	constructor(x: number, y: number) {
		// set the properties
		this.x = x;
		this.y = y;
	}

	/**
	 * @returns A new point representing the zero vector. (0, 0)
	 *
	 * @readonly
	 * @static
	 * @type {Point}
	 * @memberof Point
	 */
	static get zero(): Point {
		return new Point(0, 0);
	}

	/**
	 * @returns A new point representing the unit vector. (1, 1)
	 *
	 * @readonly
	 * @static
	 * @type {Point}
	 * @memberof Point
	 */
	static get one(): Point {
		return new Point(1, 1);
	}

	/**
	 * @returns A new point representing the up direction. (0, -1)
	 *
	 * @readonly
	 * @static
	 * @type {Point}
	 * @memberof Point
	 */
	static get up(): Point {
		return new Point(0, -1);
	}

	/**
	 * @returns A new point representing the down direction. (0, 1)
	 *
	 * @readonly
	 * @static
	 * @type {Point}
	 * @memberof Point
	 */
	static get down(): Point {
		return new Point(0, 1);
	}

	/**
	 * @returns  A new point representing the left direction. (-1, 0)
	 *
	 * @readonly
	 * @static
	 * @type {Point}
	 * @memberof Point
	 */
	static get left(): Point {
		return new Point(-1, 0);
	}

	/**
	 * @returns  A new point representing the right direction (1, 0)
	 *
	 * @readonly
	 * @static
	 * @type {Point}
	 * @memberof Point
	 */
	static get right(): Point {
		return new Point(1, 0);
	}

	/**
	 * Sets this position to a new x and y positions.
	 *
	 * @param {number} x position on the x axis.
	 * @param {number} y position on the y axis.
	 * @returns {Point} this point with the operation applied.
	 * @memberof Point
	 */
	set(x: number, y: number): Point {
		// set the new properties
		this.x = x;
		this.y = y;

		// return this
		return this;
	}

	/**
	 * Copy the values from the given point to this point.
	 *
	 * @param {Point} point the point to copy values from.
	 * @returns {Point} this point with the operation applied.
	 * @memberof Point
	 */
	copy(point: Point): Point {
		// copy the values
		this.x = point.x;
		this.y = point.y;

		return this;
	}

	/**
	 * Clone this point into a new instance using the values of this point.
	 *
	 * @returns {Point} the new instance of this point.
	 * @memberof Point
	 */
	clone(): Point {
		// create a new point using this point's values.
		return new Point(this.x, this.y);
	}

	/**
	 * Scales this point by the given x and y scalars.
	 *
	 * If the y parameter is omitted, then both x and y values are scaled by
	 * the x scalar uniformly.
	 *
	 * @param {number} x scalar to scale the x value by.
	 * @param {number} [y] scalar to scale the y value by.
	 * @returns {Point} this point with the scaling applied.
	 * @memberof Point
	 */
	scale(x: number, y?: number): Point {
		this.x *= x;
		// if y is omitted, use the x scalar.
		this.y *= y !== undefined ? y : x;

		return this;
	}

	/**
	 * Clones and scales it by the given x and y scalars.
	 *
	 * If the y parameter is omitted, then both x and y values are scaled by
	 * the x scalar uniformly.
	 *
	 * @param {number} x scalar to scale the x value by.
	 * @param {number} [y] scalar to scale the y value by.
	 * @returns {Point} the new point with the scaling applied.
	 * @memberof Point
	 */
	scaled(x: number, y?: number): Point {
		return this.clone().scale(x, y);
	}

	/**
	 * Translates this point by the given x and y values.
	 *
	 * @param {number} x value to add to the x value.
	 * @param {number} y value to add to the y value.
	 * @returns {Point} this point with the translation applied.
	 * @memberof Point
	 */
	translate(x: number, y: number): Point {
		// add the values
		this.x += x;
		this.y += y;

		return this;
	}

	/**
	 * Clones and translates this point by the given x and y values.
	 *
	 * @param {number} x value to add to the x value.
	 * @param {number} y value to add to the y value.
	 * @returns {Point} The new point with the translation applied.
	 * @memberof Point
	 */
	translated(x: number, y: number): Point {
		return this.clone().translate(x, y);
	}

	/**
	 * Rotates this point around (0,0) by the given rotation in radians.
	 *
	 * @param {number} value The rotation amount in radians.
	 * @returns {Point} this point with the rotation applied.
	 * @memberof Point
	 */
	rotate(value: number): Point {
		// get the sin and cos of the rotation
		const sin = Math.sin(value);
		const cos = Math.cos(value);

		// get the x and y values of this point
		const { x, y } = this;

		// rotate the x and y values
		this.x = x * cos - y * sin;
		this.y = x * sin + y * cos;

		return this;
	}

	/**
	 * Clones and rotates this point around (0,0) by the given rotation in
	 * radians.
	 *
	 * @param {number} value The rotation amount in radians.
	 * @returns {Point} The new point with the rotation applied.
	 * @memberof Point
	 */
	rotated(value: number): Point {
		return this.clone().rotate(value);
	}

	/**
	 * Normalizes the point to unit length.
	 *
	 * point = point / point.length
	 *
	 * @returns {Point} this point with the normalization applied.
	 * @memberof Point
	 */
	normalize(): Point {
		// get the current length squared of this point
		let len = this.lengthSquared;

		// if the length not zero (0,0)
		if (len !== 0) {
			// get the the real length
			len = Math.sqrt(len);

			// scale the point values by the length inverse to normalize
			this.x /= len;
			this.y /= len;
		}

		return this;
	}

	/**
	 * Clones and normalizes the point to unit length.
	 *
	 * point = point / point.length
	 *
	 * @returns {Point} The new point with the normalization applied.
	 * @memberof Point
	 */
	normalized(): Point {
		return this.clone().normalize();
	}

	/**
	 * The point length squared.
	 *
	 * x * x + y * y
	 *
	 * Use this instead of .length when if you just need to compare two lengths
	 * and don't need an exact length.
	 *
	 * @readonly
	 * @type {number}
	 * @memberof Point
	 */
	get lengthSquared(): number {
		return this.x * this.x + this.y * this.y;
	}

	/**
	 * The length of this point. This is the distance between this point and
	 * (0,0).
	 *
	 * @type {number}
	 * @memberof Point
	 */
	get length(): number {
		return Math.sqrt(this.lengthSquared);
	}

	/**
	 * Assigning a new length results in the point being normalized and then
	 * scaled to match the new length.
	 *
	 * @memberof Point
	 */
	set length(value: number) {
		this.normalize().scale(value);
	}

	/**
	 * @returns {number} The distance between this point and the given point.
	 * @param {Point} b The other point.
	 * @memberof Point
	 */
	distanceTo(b: Point): number {
		const x = b.x - this.x;
		const y = b.y - this.y;
		return Math.sqrt(x * x + y * y);
	}

	/**
	 * @returns {number} The distance between the given points.
	 * @param {Point} a the first point.
	 * @param {Point} b the second point.
	 * @static
	 * @memberof Point
	 */
	static distance(a: Point, b: Point): number {
		return a.distanceTo(b);
	}

	/**
	 * @returns {Point} The difference between the given points.  a - b
	 * @param {Point} a the first point.
	 * @param {Point} b the second point.
	 * @static
	 * @memberof Point
	 */
	static subtract(a: Point, b: Point): Point {
		return new Point(a.x - b.x, a.y - b.y);
	}

	/**
	 * @returns {Point} A new point pointing from a to b. (b - a).normalized()
	 * @param {Point} a the first point.
	 * @param {Point} b the second point to point at.
	 * @static
	 * @memberof Point
	 */
	static pointAt(a: Point, b: Point): Point {
		return Point.subtract(b, a).normalize();
	}

	/**
	 * @returns {Point} A new point with the absolute values of the given point.
	 * @param {Point} point the point to abs()
	 * @static
	 * @memberof Point
	 */
	static abs(point: Point): Point {
		return new Point(Math.abs(point.x), Math.abs(point.y));
	}

	/**
	 * @returns {Point} A new point containing the smallest x and y values
	 * from points a and b.
	 * @param {Point} a the first point.
	 * @param {Point} b the second point.
	 * @memberof Point
	 */
	static min(a: Point, b: Point): Point {
		return new Point(Math.min(a.x, b.x), Math.min(a.y, b.y));
	}

	/**
	 * @returns {Point} A new point containing the largest x and y values
	 * from points a and b.
	 * @param {Point} a the first point.
	 * @param {Point} b the second point.
	 * @static
	 * @memberof Point
	 */
	static max(a: Point, b: Point): Point {
		return new Point(Math.max(a.x, b.x), Math.max(a.y, b.y));
	}
}

/**
 * Rectangle object
 *
 * A 2D rectangle representing a minimum and maximum point.
 *
 * @export
 * @class Rect
 */
export class Rect {
	/**
	 * Minimum top-left point of the rectangle.
	 *
	 * @private
	 * @type {Point}
	 * @memberof Rect
	 */
	private _min: Point;

	/**
	 * Maximum bottom-right point of the rectangle.
	 *
	 * @private
	 * @type {Point}
	 * @memberof Rect
	 */
	private _max: Point;

	/**
	 * The anchor point relative to the rectangle for apply transformations.
	 *
	 * @private
	 * @type {Point}
	 * @memberof Rect
	 */
	private _anchor: Point;

	/**
	 * The y value of the top of the rectangle.
	 *
	 * @type {number}
	 * @memberof Rect
	 */
	get top(): number {
		return this._min.y;
	}
	set top(value: number) {
		this._min.y = value;
	}

	/**
	 * The y value of the bottom of the rectangle.
	 *
	 * @type {number}
	 * @memberof Rect
	 */
	get bottom(): number {
		return this._max.y;
	}
	set bottom(value: number) {
		this._max.y = value;
	}

	/**
	 * The x value of the left of the rectangle.
	 *
	 * @type {number}
	 * @memberof Rect
	 */
	get left(): number {
		return this._min.x;
	}
	set left(value: number) {
		this._min.x = value;
	}

	/**
	 * The x value of the right of the rectangle.
	 *
	 * @type {number}
	 * @memberof Rect
	 */
	get right(): number {
		return this._max.x;
	}
	set right(value: number) {
		this._max.x = value;
	}

	/**
	 * The relative anchor point of the rectangle used for transformations.
	 *
	 * @type {Point}
	 * @memberof Rect
	 */
	get anchor(): Point {
		return this._anchor.clone();
	}
	set anchor(value: Point) {
		this._anchor = value;
	}

	/**
	 * The x position of the rectangle.
	 *
	 * @type {number}
	 * @memberof Rect
	 */
	get x(): number {
		return this.left + this.width * this._anchor.x;
	}
	set x(value: number) {
		const width = this.width;
		this._min.x = value - width * this._anchor.x;
		this._max.x = this._min.x + width;
	}

	/**
	 * The y position of the rectangle
	 *
	 * @type {number}
	 * @memberof Rect
	 */
	get y(): number {
		return this.top + this.height * this._anchor.y;
	}
	set y(value: number) {
		const height = this.height;
		this.top = value - height * this._anchor.y;
		this.bottom = this.top + height;
	}

	/**
	 * The position of the rectangle
	 *
	 * @type {Point}
	 * @memberof Rect
	 */
	get position(): Point {
		return new Point(this.x, this.y);
	}
	set position(value: Point) {
		this.x = value.x;
		this.y = value.y;
	}

	/**
	 * The width of the rectangle.
	 *
	 * @type {number}
	 * @memberof Rect
	 */
	get width(): number {
		return this.right - this.left;
	}
	set width(value: number) {
		this.left = this.x - value * this._anchor.x;
		this.right = this.left + value;
	}

	/**
	 * The height of the rectangle.
	 *
	 * @type {number}
	 * @memberof Rect
	 */
	get height(): number {
		return this.bottom - this.top;
	}
	set height(value: number) {
		this.top = this.y - value * this._anchor.y;
		this.bottom = this.top + value;
	}

	/**
	 * The center point of the rectangle.
	 *
	 * @readonly
	 * @type {Point}
	 * @memberof Rect
	 */
	get center(): Point {
		return new Point(
			this.left + this.width * 0.5,
			this.top + this.height * 0.5
		);
	}

	/**
	 * The size of the rectangle. new Point(width, height)
	 *
	 * @type {Point}
	 * @memberof Rect
	 */
	get size(): Point {
		return new Point(this.width, this.height);
	}
	set size(value: Point) {
		this.width = value.x;
		this.height = value.y;
	}

	/**
	 * The minimum top-left point of the rectangle.
	 *
	 * @type {Point}
	 * @memberof Rect
	 */
	get min(): Point {
		return this._min;
	}
	set min(value: Point) {
		this._min.copy(value);
	}

	/**
	 * The maximum bottom-right point of the rectangle.
	 *
	 * @type {Point}
	 * @memberof Rect
	 */
	get max(): Point {
		return this._max;
	}
	set max(value: Point) {
		this._max = value;
	}

	/**
	 * Creates an instance of Rect.
	 * @param {number} x the x position of the rectangle.
	 * @param {number} y the y position of the rectangle.
	 * @param {number} width the width of the rectangle.
	 * @param {number} height the height of the rectangle.
	 * @param {number} [anchorX] the relative anchor point x [0, 1]
	 * @param {number} [anchorY] the relative anchor point y [0, 1]
	 * @memberof Rect
	 */
	constructor(
		x: number,
		y: number,
		width: number,
		height: number,
		anchorX?: number,
		anchorY?: number
	) {
		// forward the properties to the setter since it does the same thing
		this.set(x, y, width, height, anchorX, anchorY);
	}

	/**
	 * Sets all the values of the rectangle.
	 *
	 * @param {number} x the x position of the rectangle.
	 * @param {number} y the y position of the rectangle.
	 * @param {number} width the width of the rectangle.
	 * @param {number} height the height of the rectangle.
	 * @param {number} [anchorX] the relative anchor point x [0, 1]
	 * @param {number} [anchorY] the relative anchor point y [0, 1]
	 * @returns {Rect} This rectangle with the new values.
	 * @memberof Rect
	 */
	set(
		x: number,
		y: number,
		width: number,
		height: number,
		anchorX?: number,
		anchorY?: number
	): Rect {
		// if this rectangle does not have an anchor
		if (this._anchor === undefined)
			// set the default anchor at the center.
			this._anchor = new Point(0.5, 0.5);

		// if the anchor x parameter is included
		if (anchorX !== undefined)
			// set the new anchor x value
			this._anchor.x = anchorX;

		// if the anchor y parameter is included
		if (anchorY !== undefined)
			// set the new anchor y value
			this._anchor.y = anchorY;

		// set the top-left minimum based on the anchor and size
		this._min = new Point(
			x - width * this._anchor.x,
			y - height * this._anchor.y
		);

		// set the bottom-right max using the min point translated by the size
		this._max = this._min.translated(width, height);

		return this;
	}

	/**
	 * Translates this rectangle by the given x and y values.
	 *
	 * @param {number} x value to add to the x position.
	 * @param {number} y value to add to the y position.
	 * @returns {Rect} this rectangle with the translation applied.
	 * @memberof Rect
	 */
	translate(x: number, y: number): Rect {
		// add to the position
		this.x += x;
		this.y += y;

		return this;
	}

	/**
	 * Clones and translates this rectangle by the given x and y values.
	 *
	 * @param {number} x value to add to the x position.
	 * @param {number} y value to add to the y position.
	 * @returns {Rect} A new rectangle with the translation applied.
	 * @memberof Rect
	 */
	translated(x: number, y: number): Rect {
		return this.clone().translate(x, y);
	}

	/**
	 * Scales this rectangle by the given x and y scalars.
	 *
	 * If the y parameter is omitted, then both x and y values are scaled by
	 * the x scalar uniformly.
	 *
	 * @param {number} x scalar to scale the x value by.
	 * @param {number} [y] scalar to scale the y value by.
	 * @returns {Rect} this rectangle with the scaling applied.
	 * @memberof Rect
	 */
	scale(x: number, y?: number): Rect {
		// scale the dimensions
		this.width *= x;

		// if y is omitted, then use x
		this.height *= y !== undefined ? y : x;

		return this;
	}

	/**
	 * Clones and scales this rectangle by the given x and y scalars.
	 *
	 * If the y parameter is omitted, then both x and y values are scaled by
	 * the x scalar uniformly.
	 *
	 * @param {number} x scalar to scale the x value by.
	 * @param {number} [y] scalar to scale the y value by.
	 * @returns {Rect} A new rectangle with the scaling applied.
	 * @memberof Rect
	 */
	scaled(x: number, y?: number): Rect {
		return this.clone().scale(x, y);
	}

	/**
	 * Copy the values from the given rectangle to this rectangle.
	 *
	 * @param {Point} point the rectangle to copy values from.
	 * @returns {Point} this rectangle with the operation applied.
	 * @memberof Point
	 */
	copy(rect: Rect): Rect {
		this._min.copy(rect._min);
		this._max.copy(rect._max);
		this._anchor.copy(rect._anchor);
		return this;
	}

	/**
	 * Clone this rectangle into a new instance with the values of this rectangle.
	 *
	 * @returns {Point} the new instance of this rectangle.
	 * @memberof Point
	 */
	clone(): Rect {
		return new Rect(
			this.x,
			this.y,
			this.width,
			this.height,
			this._anchor.x,
			this._anchor.y
		);
	}

	/**
	 * @returns {Rect} set this rectangle as a rectangle that contains this
	 * rectangle and the given rectangle.
	 * @param {Rect} rect the other rectangle.
	 * @memberof Rect
	 */
	union(rect: Rect): Rect {
		// get the new minimum point
		this._min = Point.min(this._min, rect._min);
		// get the new maximum point
		this._max = Point.max(this._max, rect._max);

		return this;
	}

	/**
	 * @returns {Rect} A new rectangle that contains this rectangle and the
	 * given rectangle.
	 * @param {Rect} rect the other rectangle.
	 * @memberof Rect
	 */
	united(rect: Rect): Rect {
		return this.clone().union(rect);
	}

	/**
	 * Expands this rectangle by the given value on each side.
	 *
	 * This is not scaling but adding padding around the edges of the rectangle.
	 *
	 * @param {number} value amount to expand by.
	 * @returns {Rect} this rectangle expanded.
	 * @memberof Rect
	 */
	expand(value: number): Rect {
		// shift the top edge up
		this.top -= value;

		// shift the bottom edge down
		this.bottom += value;

		// shift the left edge left
		this.left -= value;

		// shift the right edge right
		this.right += value;

		// get the new size
		const { width, height } = this;

		// if the width is < 0
		if (width < 0)
			// make the width 0
			this.left = this.right = this.right + width * 0.5;

		// if the height is < 0
		if (height < 0)
			// make the height 0
			this.top = this.bottom = this.top + height * 0.5;

		return this;
	}

	/**
	 * Clones and expands this rectangle by the given value on each side.
	 *
	 * This is not scaling but adding padding around the edges of the rectangle.
	 *
	 * @param {number} value amount to expand by.
	 * @returns {Rect} A new rectangle with the expansion applied.
	 * @memberof Rect
	 */
	expanded(value: number): Rect {
		return this.clone().expand(value);
	}

	/**
	 * Shrinks this rectangle by the given value on each side.
	 * This is expanding the rectangle with a negative value.
	 *
	 * This is not scaling but subtracting padding around the edges of the rectangle.
	 *
	 * @param {number} value amount to shrink by.
	 * @returns {Rect} this rectangle the shrinking applied.
	 * @memberof Rect
	 */
	shrink(value: number): Rect {
		return this.expand(-value);
	}

	/**
	 * Clones and shrinks this rectangle by the given value on each side.
	 * This is expanding the rectangle with a negative value.
	 *
	 * This is not scaling but subtracting padding around the edges of the rectangle.
	 *
	 * @param {number} value amount to shrink by.
	 * @returns {Rect} A new rectangle with the shrinking applied.
	 * @memberof Rect
	 */
	shrunk(value: number): Rect {
		return this.clone().expand(-value);
	}
}
