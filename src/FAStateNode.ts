import {
	ARROW_LABEL_DIST,
	ARROW_PADDING,
	ARROW_SIZE,
	FONT_ALPHA,
	FONT_STATE,
	LINE_WIDTH,
	NODE_DISTANCE,
	NODE_ENTRY_ARROW_SIZE,
	NODE_SIZE,
} from "./constants";
import {
	FAEdgeKey,
	FAEdgeKeySetPair,
	FAResult,
	FAResultStatus,
	FAStateID,
} from "./FiniteAutomaton";
import { Point, Rect } from "./util";

import { Renderable } from "./Renderable";

/**
 * Finite Automaton State Node
 *
 * Represents an automaton state used to process input strings and render
 * to the screen.
 *
 * @export
 * @class FAStateNode
 * @extends {Renderable}
 */
export default class FAStateNode extends Renderable {
	/**
	 * The global identifier for this state (typically prefixed by q)
	 *
	 * @type {FAStateID}
	 * @memberof FAStateNode
	 */
	id: FAStateID;

	/**
	 * Indicates if this state is the entry state of the automaton.
	 *
	 * @type {boolean}
	 * @memberof FAStateNode
	 */
	isEntry: boolean;

	/**
	 * Indicates if this state is a final state of the automaton.
	 *
	 * @type {boolean}
	 * @memberof FAStateNode
	 */
	isFinal: boolean;

	/**
	 * A map of the transitions from this state.
	 *
	 * @type {Map<FAEdgeKey, FAEdgeKeySetPair>}
	 * @memberof FAStateNode
	 */
	outputEdges: Map<FAEdgeKey, FAEdgeKeySetPair>;

	/**
	 * Indicates if predraw() has been called for this node.
	 *
	 * @private
	 * @type {boolean}
	 * @memberof FAStateNode
	 */
	private _predrawn: boolean;

	/**
	 * Indicates if draw() has been called for this node.
	 *
	 * @private
	 * @type {boolean}
	 * @memberof FAStateNode
	 */
	private _drawn: boolean;

	/**
	 * Indicates if this node can be reached by the entry node.
	 * This is calculated during the predraw.
	 *
	 * @private
	 * @type {boolean}
	 * @memberof FAStateNode
	 */
	private _reachable: boolean;

	/**
	 * The angle in radians of the tangent of this node's position around the
	 * automaton circle.
	 *
	 * This is used for placing the loop transition arrow.
	 *
	 * @private
	 * @type {number}
	 * @memberof FAStateNode
	 */
	private _tangent: number;

	/**
	 * The last input string given in the last process() call.
	 *
	 * This is used for NFAs to prevent infinite recursion.
	 *
	 * @private
	 * @type {string}
	 * @memberof FAStateNode
	 */
	private _lastProcessedInput: string;

	/**
	 * FAStateNode constructor
	 *
	 * @param id the unique state ID (typically prefixed by q)
	 */
	constructor(id: FAStateID) {
		// initialize bounding rect with a size of 0 and the anchor in the center
		super(0, 0, 0, 0, 0.5, 0.5);

		// initialize properties

		this.id = id;
		this.outputEdges = new Map<FAEdgeKey, FAEdgeKeySetPair>();

		this._predrawn = false;
		this._drawn = false;
		this._reachable = false;
		this._lastProcessedInput = "";
	}

	/**
	 * Recursively process a string and returns the result.
	 *
	 * @param input input string to process
	 * @returns the FAResult containing if the input was accepted or rejected
	 *  and the final state.
	 */
	process(input: string): FAResult {
		// we reached the end of the input string
		if (input.length === 0)
			return {
				// accept if this is a final state
				status: this.isFinal
					? FAResultStatus.ACCEPTED
					: FAResultStatus.REJECTED,
				finalState: this,
			};

		// fail if this node has already seen this exact input.
		// this prevents infinite recursion.
		if (this._lastProcessedInput && input === this._lastProcessedInput) {
			return {
				status: FAResultStatus.REJECTED,
				finalState: this,
			};
		}

		// split the first char from the rest of the input
		const [char, next] = [input.slice(0, 1), input.slice(1)];

		// iterate over all the transition edges

		// get the iterator for the map
		const iter = this.outputEdges.values();

		// get the first transition
		let cur = iter.next();

		// while there is another element in the iterator
		while (!cur.done) {
			// if the current transition accepts an empty string
			if (cur.value.values.has("e")) {
				// we are not going to pop the first char off the input so we
				// store the input on the node so if we come back to it, we can
				// break it and prevent an infinite loop.
				this._lastProcessedInput = input;

				// go to the node and send the unmodified input
				const result: FAResult = cur.value.node.process(input);

				// if the input was recursively accepted, return the result
				if (result.status === FAResultStatus.ACCEPTED) return result;
			}

			// if the current transition accepts the current character
			if (cur.value.values.has(char)) {
				// go to the state and send it the input without the first char
				const result: FAResult = cur.value.node.process(next);

				// if the input was recursively accepted, return the result
				if (result.status === FAResultStatus.ACCEPTED) return result;
			}

			// iterate to the next transition
			cur = iter.next();
		}

		// we checked all our transitions and none of them accept the current
		// character, so return that the input is rejected.
		return {
			status: FAResultStatus.REJECTED,
			finalState: this,
		};
	}

	/**
	 * The bounding box of this node.
	 *
	 * @readonly
	 * @type {Rect}
	 * @memberof FAStateNode
	 */
	get bounds(): Rect {
		// expand the inherited bounding box to account for the line width
		const bounds = super.bounds.expand(LINE_WIDTH);

		// if this is the entry state
		if (this.isEntry)
			// add some extra padding to the top for the entry arrow
			bounds.top -= NODE_ENTRY_ARROW_SIZE;

		// return the final bounding rect
		return bounds;
	}

	/**
	 * Recursively determines the max depth or "reach" of the graph.
	 *
	 * @private
	 * @returns the reach for this node
	 */
	private reach(): number {
		// start with a length of 1 since this node is included.
		let length = 1;

		// we have counted this node, so flag it so we don't count it again later.
		this._reachable = true;

		// now we recursively call reach() on the transition nodes

		// for each transition node
		this.outputEdges.forEach((edge) => {
			// skip nodes that have already been counted
			if (edge.node._reachable) return;

			// add the reach of the transition node
			length += edge.node.reach();
		});

		// return the final total reach for this node
		return length;
	}

	/**
	 * Recursively call predraw all the connected nodes.
	 */
	predraw(): void {
		this._predraw(0, this.reach(), this.rect.clone());
	}

	/**
	 * Inner predraw recursion method
	 *
	 * @param index the count of how many nodes have been predrawn so far
	 * @param total the total number of nodes to predraw
	 * @param rect the previous node rect to add to
	 */
	private _predraw(index: number, total: number, rect: Rect): number {
		// mark this node so we can skip it later
		this._predrawn = true;

		// calculate the rotation difference between each node
		const angleStep = Math.PI * 2 * (1 / total);

		// calculate this node's tangent angle
		this._tangent = angleStep * ((total + index - 0.5) % total);

		// for each transition node
		this.outputEdges.forEach((edge) => {
			// skip nodes that have already been predrawn
			if (edge.node._predrawn) return;

			// calculate the new direction to move in
			// and scale it by the distance we want to move
			const angle = Point.right.rotate(angleStep * index).scale(NODE_DISTANCE);
			// .scale(Math.sqrt(index) * NODE_DISTANCE);

			// move the rect to the new position
			rect.translate(angle.x, angle.y);

			// have the target node copy the position from the rect
			edge.node.rect.copy(rect);

			// recursively go to the current nodes transition nodes and predraw them
			index++;
			index = edge.node._predraw(index, total, rect);
		});

		// return the new count of predrawn nodes
		return index;
	}

	/**
	 * Draw this Renderable to the given context.
	 * @param ctx the canvas context to render to
	 */
	draw(ctx: CanvasRenderingContext2D): void {
		// this.drawDebug(ctx);

		// start a new path
		ctx.beginPath();

		// get the position and dimensions of our node
		const { x, y, top, width, height } = this.rect;

		// get the circle radius
		const radius = Math.min(width * 0.5, height * 0.5);

		// trace outer circle
		ctx.ellipse(x, y, radius, radius, 0, 0, Math.PI * 2);

		// draw a line along the circle path
		ctx.stroke();

		// draw inner circle if this is a final state
		if (this.isFinal) {
			// start a new path
			ctx.beginPath();
			// trace the inner circle
			ctx.ellipse(x, y, radius * 0.8, radius * 0.8, 0, 0, Math.PI * 2);
			// draw a line along the inner circle path
			ctx.stroke();
		}

		// draw entry arrow if this is the entry node
		if (this.isEntry) {
			// start a new path
			ctx.beginPath();

			// draw the two lines

			ctx.moveTo(x, top);
			ctx.lineTo(x - NODE_ENTRY_ARROW_SIZE, top - NODE_ENTRY_ARROW_SIZE);
			ctx.moveTo(x, top);
			ctx.lineTo(x + NODE_ENTRY_ARROW_SIZE, top - NODE_ENTRY_ARROW_SIZE);

			// draw a line along the path
			ctx.stroke();
		}

		// update font properties
		ctx.font = FONT_STATE;
		ctx.textBaseline = "middle";
		ctx.textAlign = "center";

		// draw the state ID text
		ctx.fillText(this.id, x, y);

		// close any remaining paths
		ctx.closePath();

		// flag this node as drawn
		this._drawn = true;

		// draw the transition arrows and nodes
		this.drawEdges(ctx);
	}

	/**
	 * Draw the transition arrows and nodes
	 *
	 * @private
	 * @param ctx the canvas context to render to
	 */
	private drawEdges(ctx: CanvasRenderingContext2D): void {
		// get the center position of this node
		const { center } = this.rect;

		// for each transition
		this.outputEdges.forEach((edge) => {
			// draw the transition arrow
			this.drawArrow(
				ctx,
				// from the center of this node
				center,
				// to the center of the other node
				edge.node.rect.center,
				// join the transitions together with a ","
				Array.from(edge.values.values()).join(",")
			);

			// draw the transition node if it hasn't already been drawn
			if (!edge.node._drawn) edge.node.draw(ctx);
		});
	}

	/**
	 * Draw the transition arrow
	 * @param ctx the canvas context to draw to
	 * @param start the starting point of the arrow
	 * @param end the ending point of the arrow
	 * @param label the label text of the arrow
	 */
	private drawArrow(
		ctx: CanvasRenderingContext2D,
		start: Point,
		end: Point,
		label: string
	): void {
		// get the total distance between the start and end positions
		const dist = start.distanceTo(end);

		// if this distance is 0, this means the arrow loops back to the same node
		if (dist <= 0.001) {
			// get the angle off the node to place the arrow
			const angle = Point.up.rotate(
				this._tangent +
					// add a little extra to the tangent if this is the entry node
					// so we don't overlap the entry arrow
					Math.PI * (this.isEntry ? -0.16 : 0.1)
			);

			// angle difference between the start and end of the loop
			const angleDiff = 0.1;

			// angle for the start of the loop
			const angleStart = angle.rotated(Math.PI * angleDiff);

			// angle for the end of the loop
			const angleEnd = angle.rotated(-Math.PI * angleDiff);

			// starting position of the loop
			const offsetStart = angleStart
				.scaled(NODE_SIZE * 0.5 + ARROW_PADDING)
				.translate(start.x, start.y);

			// ending position of the loop
			const offsetEnd = angleEnd
				.scaled(NODE_SIZE * 0.5 + ARROW_PADDING)
				.translate(end.x, end.y);

			// bezier curve point 1
			const curvePoint1 = angleStart
				.scaled(NODE_SIZE * 0.65)
				.translate(offsetStart.x, offsetStart.y);

			// bezier curve point 2
			const curvePoint2 = angleEnd
				.scaled(NODE_SIZE * 0.65)
				.translate(offsetEnd.x, offsetEnd.y);

			// the two lines for the arrow head and the end
			const arrowhead = [
				angleEnd
					.rotated(Math.PI * 0.25)
					.scale(ARROW_SIZE)
					.translate(offsetEnd.x, offsetEnd.y),
				angleEnd
					.rotated(Math.PI * -0.25)
					.scale(ARROW_SIZE)
					.translate(offsetEnd.x, offsetEnd.y),
			];

			// the label text position
			const textpos = angle
				.scale(NODE_SIZE + ARROW_LABEL_DIST + ARROW_LABEL_DIST)
				.translate(start.x, start.y);

			// start a new path
			ctx.beginPath();
			// start the line at the arrow start position
			ctx.moveTo(offsetStart.x, offsetStart.y);

			// create a bezier curve from the start position to the end
			ctx.bezierCurveTo(
				curvePoint1.x,
				curvePoint1.y,
				curvePoint2.x,
				curvePoint2.y,
				offsetEnd.x,
				offsetEnd.y
			);

			// start a line at the end of the arrow
			ctx.moveTo(offsetEnd.x, offsetEnd.y);

			// trace the first arrow head line
			ctx.lineTo(arrowhead[0].x, arrowhead[0].y);

			// move back to the end
			ctx.moveTo(offsetEnd.x, offsetEnd.y);

			// trace the second arrow head line
			ctx.lineTo(arrowhead[1].x, arrowhead[1].y);

			// draw the path
			ctx.stroke();

			// close the path
			ctx.closePath();

			// draw transition arrow label
			ctx.font = FONT_ALPHA;
			ctx.textBaseline = "middle";
			ctx.textAlign = "center";
			ctx.fillText(label, textpos.x, textpos.y);

			return;
		}

		// draw the arrow between one node and a different node

		// the angle diff used to space out two opposing arrows
		const angleDiff = 0.05;

		// get the angle vector from the start node to the end node
		const angle = Point.pointAt(start, end);

		// get the position to start the arrow
		const offsetstart = angle
			.rotated(-Math.PI * angleDiff)
			.scale(NODE_SIZE * 0.5 + ARROW_PADDING)
			.translate(start.x, start.y);

		// get the position of the end of the arrow
		const offsetend = angle
			.rotated(-Math.PI * (1 - angleDiff))
			.scale(NODE_SIZE * 0.5 + ARROW_PADDING)
			.translate(end.x, end.y);

		// get the mid point of the arrow to put the label text on
		const middle = angle
			.scaled(offsetstart.distanceTo(offsetend) * 0.5)
			.translate(offsetstart.x, offsetstart.y);

		// offset away from the midpoint to position the label text
		const textpos = angle
			.rotated(-Math.PI * 0.5)
			.scale(ARROW_LABEL_DIST)
			.translate(middle.x, middle.y);

		// the two points used to make the arrow head
		const arrowhead = [
			angle
				.rotated(-Math.PI * 0.75)
				.scale(ARROW_SIZE)
				.translate(offsetend.x, offsetend.y),
			angle
				.rotated(Math.PI * 0.75)
				.scale(ARROW_SIZE)
				.translate(offsetend.x, offsetend.y),
		];

		// start a new path
		ctx.beginPath();

		// start the line at the starting position
		ctx.moveTo(offsetstart.x, offsetstart.y);

		// trace a line to the ending position
		ctx.lineTo(offsetend.x, offsetend.y);

		// start a line at the ending position
		ctx.moveTo(offsetend.x, offsetend.y);

		// trace the first arrow line
		ctx.lineTo(arrowhead[0].x, arrowhead[0].y);

		// go back to the end position
		ctx.moveTo(offsetend.x, offsetend.y);

		// trace the second arrow line
		ctx.lineTo(arrowhead[1].x, arrowhead[1].y);

		// draw the path
		ctx.stroke();

		// close the path
		ctx.closePath();

		// draw transition arrow text
		ctx.font = FONT_ALPHA;
		ctx.textBaseline = "middle";
		ctx.textAlign = "center";
		ctx.fillText(label, textpos.x, textpos.y);
	}
}
