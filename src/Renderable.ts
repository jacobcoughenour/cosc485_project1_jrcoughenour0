import { Point, Rect } from "./util";

/**
 * Renderable
 *
 * Used to render objects to the screen.
 *
 * @export
 * @abstract
 * @class Renderable
 */
export abstract class Renderable {
	/**
	 * The object transformation rectangle
	 *
	 * @type {Rect}
	 * @memberof Renderable
	 */
	rect: Rect;

	/**
	 * Renderable Default Constructor
	 *
	 * use this to initialize the transform rect
	 *
	 * @param {number} x transform x position
	 * @param {number} y transform y position
	 * @param {number} width object width
	 * @param {number} height object height
	 * @param {number} [anchorX] anchor x position
	 * @param {number} [anchorY] anchor y position
	 * @memberof Renderable
	 */
	constructor(
		x: number,
		y: number,
		width: number,
		height: number,
		anchorX?: number,
		anchorY?: number
	) {
		// create the transform rect
		this.rect = new Rect(x, y, width, height, anchorX, anchorY);
	}

	/**
	 * The bounding box of this object
	 *
	 * @readonly
	 * @type {Rect}
	 * @memberof Renderable
	 */
	get bounds(): Rect {
		// by default we just use the transform rect as the bounds

		// clone the transform rect so it acts as a readonly
		const bounds = this.rect.clone();
		bounds.anchor = Point.zero;
		return bounds;
	}

	/**
	 * Called to update transform rect before draw()
	 *
	 * @returns {void}
	 * @memberof Renderable
	 */
	predraw(): void {
		return;
	}

	/**
	 * Draw a debug bounding box for the object.
	 * @param ctx canvas context to draw to
	 */
	drawDebug(ctx: CanvasRenderingContext2D): void {
		const styleBefore = ctx.strokeStyle;
		const widthBefore = ctx.lineWidth;
		ctx.strokeStyle = "red";
		ctx.lineWidth = 1;
		const { x, y, width, height } = this.bounds;
		ctx.strokeRect(x, y, width, height);
		ctx.strokeStyle = styleBefore;
		ctx.lineWidth = widthBefore;
	}

	/**
	 * Draws the object to the given context.
	 * @param ctx canvas context to draw to
	 */
	abstract draw(ctx: CanvasRenderingContext2D): void;
}
