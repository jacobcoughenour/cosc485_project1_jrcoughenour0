import { LINE_WIDTH, NODE_SIZE } from "./constants";

import FAStateNode from "./FAStateNode";
import { Rect } from "./util";
import { Renderable } from "./Renderable";

/**
 * Finite Automaton State ID
 *
 * typically prefixed with q
 */
export type FAStateID = string;

/**
 * Finite Automaton Transition Value
 */
export type FAEdgeValue = string;

/**
 * Finite Automaton Transition Key
 */
export type FAEdgeKey = string;

/**
 * Finite Automaton Transition Key-Set Pair
 */
export type FAEdgeKeySetPair = {
	/**
	 * values this transition accepts
	 */
	values: Set<FAEdgeValue>;
	/**
	 * the state this transition goes to
	 */
	node: FAStateNode;
};

/**
 * Finite Automaton stream process status
 */
export enum FAResultStatus {
	/**
	 * The input string is accepted by the Finite Automaton
	 */
	ACCEPTED,
	/**
	 * The input string is rejected by the Finite Automaton
	 */
	REJECTED,
}

/**
 * Finite Automaton input processing result structure
 */
export type FAResult = {
	/**
	 * Indicates if the input string is accepted or rejected
	 */
	status: FAResultStatus;
	/**
	 * The final state node the input was accepted or rejected by
	 */
	finalState: FAStateNode;
};

/**
 * Finite Automaton processor
 *
 * Represents a Finite Automaton used to process input strings and render
 * to the screen.
 *
 * @export
 * @class FiniteAutomaton
 * @extends {Renderable}
 */
export default class FiniteAutomaton extends Renderable {
	/**
	 * The unique ID for the entry state.
	 *
	 * @private
	 * @type {FAStateID}
	 * @memberof FiniteAutomaton
	 */
	private _entryID: FAStateID;

	/**
	 * A map of all the states in the automaton.
	 *
	 * @private
	 * @type {Map<FAStateID, FAStateNode>}
	 * @memberof FiniteAutomaton
	 */
	private _states: Map<FAStateID, FAStateNode>;

	/**
	 * A map of all the final states in the automaton.
	 *
	 * @private
	 * @type {Map<FAStateID, FAStateNode>}
	 * @memberof FiniteAutomaton
	 */
	private _finalStates: Map<FAStateID, FAStateNode>;

	/**
	 * A map of all the transitions in the automaton.
	 *
	 * @private
	 * @type {Map<FAEdgeKey, Set<FAEdgeValue>>}
	 * @memberof FiniteAutomaton
	 */
	private _edges: Map<FAEdgeKey, Set<FAEdgeValue>>;

	/**
	 * A set of the automaton alphabet.
	 *
	 * @private
	 * @type {Set<FAEdgeValue>}
	 * @memberof FiniteAutomaton
	 */
	private _alphabet: Set<FAEdgeValue>;

	/**
	 * Indicates if the renderable needs predraw all the nodes.
	 *
	 * @private
	 * @type {boolean}
	 * @memberof FiniteAutomaton
	 */
	private _needsPredraw: boolean;

	/**
	 * Indicates if the content has changed and the bounds need recalculated.
	 *
	 * @private
	 * @type {boolean}
	 * @memberof FiniteAutomaton
	 */
	private _boundsDirty: boolean;

	constructor() {
		// start with a rect size of 0
		super(0, 0, 0, 0, 0, 0);

		// initialize the properties

		this._states = new Map<FAStateID, FAStateNode>();
		this._finalStates = new Map<FAStateID, FAStateNode>();
		this._edges = new Map<FAEdgeKey, Set<FAEdgeValue>>();
		this._alphabet = new Set<FAEdgeValue>();
		this._needsPredraw = true;
		this._boundsDirty = true;
	}

	/**
	 * Set the automaton alphabet overriding the existing one.
	 * @param alphabet an array of FAEdgeValues representing the new alphabet
	 */
	setAlphabet(alphabet: FAEdgeValue[]): void {
		// clear the alphabet set
		this._alphabet.clear();

		// for each of the new alphabet
		alphabet.forEach((e) => {
			// if the element is a duplicate, throw an error
			if (this._alphabet.has(e)) throw Error("invalid alphabet");
			// add element to the alphabet set
			this._alphabet.add(e);
		});

		// mark dirty for a fresh render
		this.markDirty();
	}

	/**
	 * Get a state node in the automaton
	 * @param id the unique state ID
	 * @returns the state node for the given ID
	 */
	private getState(id: FAStateID): FAStateNode {
		return this._states.get(id);
	}

	/**
	 * Get multiple state nodes in the automaton
	 * @param ids the unique state IDs
	 * @returns the state nodes for each given ID
	 */
	private getStates(ids: FAStateID[]): FAStateNode[] {
		return ids.map((id) => this.getState(id));
	}

	/**
	 * @param id the unique state ID
	 * @returns if the state exists in this automaton
	 */
	hasState(id: FAStateID): boolean {
		return this._states.has(id);
	}

	/**
	 * Adds a new state to the automaton.
	 *
	 * @throws Error if the state ID already exists in the automaton.
	 * @param id the unique state ID to add
	 */
	addState(id: FAStateID): void {
		// check if the id already exists in this automaton
		if (this.hasState(id)) throw Error("state already exists");

		// create a new state node of this id
		const node = new FAStateNode(id);

		// register the state node and its id
		this._states.set(id, node);

		// mark dirty for a redraw
		this.markDirty();
	}

	/**
	 * Set the states for the automaton.
	 *
	 * @throws Error if there are duplicate state ids
	 * @param ids new unique state ids
	 */
	setStates(ids: FAStateID[]): void {
		// clear previous map of states
		this._states.clear();

		// add each new state
		ids.forEach((id) => this.addState(id));
	}

	/**
	 * Set the entry state.
	 *
	 * @param id the ID of the entry state
	 */
	setEntryState(id: FAStateID): void {
		// if an entry state is already set
		if (this.hasState(this._entryID))
			// update it so it is not the entry state
			this.getState(this._entryID).isEntry = false;

		// update the entry state id
		this._entryID = id;

		// update the node so it is the entry state
		this.getState(id).isEntry = true;

		// mark dirty for redraw
		this.markDirty();
	}

	/**
	 * Set the final states.
	 * @param ids unique ids of the final states
	 */
	setFinalStates(ids: FAStateID[]): void {
		// update the current final states so they are not final
		this._finalStates.forEach((node) => (node.isFinal = false));

		// update all the new final states to be final
		this.getStates(ids).forEach((node) => (node.isFinal = true));
	}

	/**
	 * Add a transition to the automaton.
	 * @param from transition starting state
	 * @param value transition value
	 * @param to transition ending state
	 * @throws Error if the transition already exists in the automaton
	 * @throws Error if the *from* or *to* state IDs do not exist in the automaton
	 */
	addTransition(from: FAStateID, value: FAEdgeValue, to: FAStateID): void {
		// if the from state id doesn't exist
		if (!this.hasState(from)) throw Error("unknown state");
		// if the to state id doesn't exist
		if (!this.hasState(to)) throw Error("unknown state");

		// create the transition edge key for the map
		const edge: FAEdgeKey = [from, to].join(",");

		// if a transition does not already exist with the same key
		if (!this._edges.has(edge))
			// create a transition entry with a new set
			this._edges.set(edge, new Set<FAEdgeValue>());

		// get the transition value set
		const set = this._edges.get(edge);

		// this transition already exists
		if (set.has(value)) throw Error("transition already exists");

		// add the new value to the transition set
		set.add(value);

		// update the transition in the parent node's transition map
		this.getState(from).outputEdges.set(edge, {
			values: set,
			node: this.getState(to),
		});

		// mark dirty for redraw
		this.markDirty();
	}

	/**
	 * Process an input string with the finite automaton.
	 *
	 * @param input input string to process in the automaton
	 * @returns processing result
	 */
	process(input: string): FAResult {
		// if the automaton does not have an entry state
		if (!this.hasState(this._entryID))
			// reject the input
			return {
				status: FAResultStatus.REJECTED,
				finalState: null,
			};

		// get the entry state and recursively process the input
		return this.getState(this._entryID).process(input);
	}

	/**
	 * Marks the whole object dirty so nodes need predrawn and bounds recalculated.
	 */
	private markDirty(): void {
		this._boundsDirty = true;
		this._needsPredraw = true;
	}

	/**
	 * If the bounds or node positions are marked as dirty and need updated.
	 */
	private isDirty(): boolean {
		// it is dirty if the bounds are dirty or we haven't predrawn yet
		return this._boundsDirty || this._needsPredraw;
	}

	/**
	 * Predraws the Renderable.
	 *
	 * Calculates the placement of all the state nodes.
	 */
	predraw(): void {
		// skip if not dirty
		if (!this._needsPredraw) return;

		// skip if there are no states
		if (this._states.size === 0) return;

		// get the entry state
		const state = this.getState(this._entryID);

		// place the entry state at the origin point of this rect
		state.rect.set(
			this.rect.center.x,
			this.rect.center.y,
			NODE_SIZE,
			NODE_SIZE
		);

		// have the entry recursively predraw the whole graph
		state.predraw();

		// mark the predraw as no longer dirty
		this._needsPredraw = false;
	}

	/**
	 * Renderable bounding box
	 *
	 * @readonly
	 * @type {Rect}
	 * @memberof FiniteAutomaton
	 */
	get bounds(): Rect {
		// if the bounds need recalculated
		if (this.isDirty() && this._states.size !== 0) {
			// predraw the nodes into position
			this.predraw();

			// iterate over all the state nodes
			const iter = this._states.values();

			// get the first node
			let cur = iter.next();

			// get the first bounding box
			const bounds: Rect = cur.value.bounds;

			// get the next node
			cur = iter.next();

			// for each node
			while (!cur.done) {
				// create a new bounding box containing the current
				// one and the node's bounding box
				bounds.union(cur.value.bounds);

				// go to the next node
				cur = iter.next();
			}

			// unmark bounds as dirty
			this._boundsDirty = false;

			// set the new bounding box as the rect
			this.rect = bounds;
		}

		// return a readonly clone of the new bounding box
		return this.rect.clone();
	}

	/**
	 * Draw this Renderable
	 * @param ctx the canvas context to draw to
	 */
	draw(ctx: CanvasRenderingContext2D): void {
		// set the global line styles
		ctx.lineWidth = LINE_WIDTH;
		ctx.lineCap = "round";

		// get the entry state
		const state = this.getState(this._entryID);

		// recursively draw the graph
		state.draw(ctx);
	}
}
