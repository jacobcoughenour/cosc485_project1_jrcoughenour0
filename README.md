## COSC485_project1_jrcoughenour0

### Setup

To run the project from the source, you will need [node.js](https://nodejs.org/en/) and [yarn](https://yarnpkg.com/) installed.

Run the ```yarn``` command in the project root to install all the dependencies.

Use the start script the start the project

for Windows
```
.\start.bat file1 file2 file3
```

for Linux
```
./start.sh file1 file2 file3
```